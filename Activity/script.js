async function fetchData(){

	const result = await fetch("https://jsonplaceholder.typicode.com/todos");
	const showData = await result.json();
	console.log(showData);

	const showTitle = showData.map((json) => 'Title: ' + json.title);
	console.log(showTitle);
}
fetchData();

fetch("https://jsonplaceholder.typicode.com/todos/2")
.then((response) => response.json())
.then((json) => console.log(json));

fetch("https://jsonplaceholder.typicode.com/todos/2")
.then((response) => response.json())
.then((json) => {
	console.log(`Title: ${json.title} | Completed ${json.completed}`)
});

async function fetchRequestPOST(){

	fetch("https://jsonplaceholder.typicode.com/todos",
		{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				userId: 2,
				completed: true,
				title: "This is a New Post"
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
}
fetchRequestPOST();

async function fetchRequestPUT(){

	fetch("https://jsonplaceholder.typicode.com/todos/2",
		{
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				title: "Updated To Do Title"
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
}
fetchRequestPUT();

async function ChangeToDoStructure(){

	fetch("https://jsonplaceholder.typicode.com/todos/2",
		{
			method: "PUT",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				title: "Title for Changed To Do List",
				description: "Description for Changed To Do List",
				status: "completed",
				date_completed: "2022-08-27",
				userId: 22
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
}
ChangeToDoStructure();

async function fetchRequestPATCH(){

	fetch("https://jsonplaceholder.typicode.com/todos/2",
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				title: "Patched Title"
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
}
fetchRequestPATCH();

async function changeStatusAndDate(){

	fetch("https://jsonplaceholder.typicode.com/todos/2",
		{
			method: "PATCH",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				completed: true,
				date: "2022-08-27"
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
}
changeStatusAndDate();

async function deleteRequest(){

	fetch("https://jsonplaceholder.typicode.com/todos/2",
		{
			method: "DELETE"
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))
console.log("Deleted To Do");
}
deleteRequest();



