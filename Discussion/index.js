// Javascript Synchronous and Asynchronous

// JS is by default synchronous, it means that only one statement is executed at a time.

/*console.log("Hello World");
consol.log("Hello");
console.log("Bye-bye");*/

/*console.log("Hello World");
for(i = 0; i <= 1000; i++){
	console.log(i);
};
console.log("Hello Again");*/

//Asynchronous means that we can proceed to execute other statements/code block, while time consuming code is running in the background.

// Getting All Posts
// The Fetch API allows you to asynchronously request for a resources(data).
// A "promise" is an object that represents the eventual completion (or failure) of an asynchronous function and its resulting value.

/*
	Syntax:
		fetch("URL");
*/

// A promise may be in one of the 3 possible states: fulfilled, rejected, or pending
console.log(fetch("https://jsonplaceholder.typicode.com/posts"));

/*
	Syntax:
		fetch("URL")
		.then((response) => {//line of codes})
*/

//fetch("https://jsonplaceholder.typicode.com/posts")
// By using the response.status we only check if the promise is fulfilled or rejected
// ".then" method captures the "response" object and returns another "promise" wich will eventually be "resolved" or "rejected" 

// Use the "json" method from the "response" object to convert the data retrieved into JSON format to be used in our application.
//.then((response) => response.json())

// Uisng multiple "then" methods that creates a "promise chain".
//.then((json) => console.log(json));


// Display each title of the posts
/*.then((json) => {
	json.forEach(posts => console.log(posts.title))
})
*/
async function fetchData(){

	// Await - waits for the "fetch" method to complete, then it stores the value in the result variable
	let result = await fetch("https://jsonplaceholder.typicode.com/posts");

	// Result returned by fetch is a promise returned.
	console.log(result);
	// The returned "response" is an object
	console.log(typeof result);
	// We cannot access the content of the "response" by directly accessing it's body properly
	console.log(result.body);

	// Converts a data from the "response" object to JSON format.
	let json = await result.json()
	console.log(json)
}
fetchData();

// Getting a specific post
// (retrieve, /posts/:id, GET)


// ":id" is a wildcard where you can put the unique identifier as value to show specific data/resource
fetch("https://jsonplaceholder.typicode.com/posts/10")
.then((response) => response.json())
.then((json) => console.log(json));

// Creating a post
/*
	Syntax:
		fetch("URL", options)
		.then((response) =? {line of code})
		.then((json) => {line of code})
*/

fetch("https://jsonplaceholder.typicode.com/posts",
		{
			// HTTP Method
			method: "POST",
			//Specifies the content that it will pass in JSON format
			headers: {
				"Content-Type": "application/json"
			},
			// Sets the content/bpdy data of the "Request" object to be sent to the backend/server
			body: JSON.stringify({
				title: "New Post",
				body: "Hello Batch 197",
				userId: 1
			})
		}
	)
// response of the server based on the request
.then((response) => response.json())
.then((json) => console.log(json))


// Updating a post
// (update, /posts/:id, PUT)

fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Updated post",
		body: "Hello again Batch-197!",
		userId: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

// PATCH 

fetch("https://jsonplaceholder.typicode.com/posts/10", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		title: "Corrected Yarn"
	})
})
.then((response) => response.json())
.then((json) => console.log(json))

// Deleting a post
// (delete, /posts/:id, DELETE)

fetch("https://jsonplaceholder.typicode.com/posts/10",{
	method: "DELETE"
})
